package com.anappsdeveloper.ispend.repository

import com.anappsdeveloper.ispend.data.model.DayMonthModel
import com.anappsdeveloper.ispend.domain.ISpend
import com.anappsdeveloper.ispend.room.*
import kotlinx.coroutines.flow.Flow

class MainRepository
constructor(
    private val mainDao: MainDao
){
    suspend fun insertMainEntity(ISpendList: List<ISpend>){
        try{
            for(spend in ISpendList){
                mainDao.insertMainEntity(MainEntity(0,spend.item,spend.price,spend.cat, spend.date,spend.isExpenses))
            }
        }catch (error: Throwable){
            throw ItemError("Unable to refresh title", error)
        }
    }

     fun getAllMainEntity(limit: Int, offSet :Int):Flow<List<MainEntity>> = mainDao.getAllMainEntity(limit,offSet)

     suspend fun getMontlyMainEntity(): List<DayMonthModel> = mainDao.getMonthlyMainEntity()

    suspend fun getDailyMainEntity(limit: Int, offSet :Int):List<DayMonthModel> = mainDao.getDailyMainEntity(limit,offSet)

    suspend fun updateMainEntityById(id:Int, item:String, price:Int, cat:String, date:String) = mainDao.updateMainEntityById(id,item,price,cat,date)

    suspend fun deleteMainEntityById(id:Int) = mainDao.deleteMainEntityById(id)

    fun searchAllMainEntity(query:String):Flow<List<MainEntity>> = mainDao.searchAllMainEntity(query)

     fun getAllSum() = mainDao.getAllSum()

}
class ItemError(message: String, cause: Throwable) : Throwable(message, cause)
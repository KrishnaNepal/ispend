package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.foundation.ScrollableRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.Composable
import com.anappsdeveloper.ispend.data.ExpensesCategory
import com.anappsdeveloper.ispend.data.IncomeCategory
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.CategoryChip

@Composable
fun CatChipSet(
    selectedCategory: String,
    filterScrollPosition: Float,
    onSelectedCategoryChanged: (String) -> Unit,
    homeViewModel: HomeViewModel,
    application:MyApp
) {
        val scrollState = rememberScrollState()
        ScrollableRow(
            scrollState = scrollState
        ) {
            scrollState.scrollTo(filterScrollPosition) //show respective position after rotate

            if(homeViewModel.expensesMode){
                for (category in ExpensesCategory.values()) {
                    CategoryChip(
                        category = category.value,
                        isSelected = selectedCategory == category.value,
                        onSelectedCategoryChanged = {
                            onSelectedCategoryChanged(it)
                            homeViewModel.filterScrollPosition = scrollState.value
                        }
                    ) {}
                }
            }else{
                for (category in IncomeCategory.values()) {
                    CategoryChip(
                        category = category.value,
                        isSelected = selectedCategory == category.value,
                        onSelectedCategoryChanged = {
                            onSelectedCategoryChanged(it)
                            homeViewModel.filterScrollPosition = scrollState.value
                        }
                    ) {}
                }
            }
        }
}
package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun <T> CardListItem(
    item: List<T>,
    listItem: @Composable (T) -> Unit
) {
    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        items(items = item, itemContent = {
            listItem(it)
        })
    }
}
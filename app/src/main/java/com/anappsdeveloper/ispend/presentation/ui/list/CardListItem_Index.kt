package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.anappsdeveloper.ispend.data.DateCategory

@Composable
fun <T> CardListItem_Index(
    viewModel:ListViewModel,
    items: List<T>,
    listItem: @Composable (T,Int) -> Unit
) {

    LazyColumn(modifier = Modifier . fillMaxSize()) {
        itemsIndexed(items = items){ index, item ->
            listItem(item,index)

            if(viewModel.selectedCategory == DateCategory.ALL){
            if((index + 1) == (viewModel.page.value * PAGE_SIZE)) //does list reach to limit
                viewModel.nextPage()
            }else{
                if((index + 1) == (viewModel.page_daily.value * PAGE_SIZE)) //does list reach to limit
                    viewModel.nextPage_daily()
            }
        }
    }
}
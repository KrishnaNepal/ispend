package com.anappsdeveloper.ispend.presentation.component.shimmer

import androidx.compose.animation.transition
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.WithConstraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.anappsdeveloper.ispend.presentation.component.shimmer.ShimmerAnimationDefinitions.*

@Composable
 fun LoadingListShimmer(
    rowHeight: Dp,
    padding:Dp = 16.dp
 ){
     WithConstraints() { /** calculate width of screen*/

     /** remove padding*/
    /* val cardWidthPx = with(AmbientDensity.current){
             ((maxWidth - (padding*2)).toPx())
         }*/

         val cardAnimationDefinition = remember{ /** While recompose remember previous value*/
             ShimmerAnimationDefinitions(
                 widthPx = maxWidth.value,
                 heightPx = rowHeight.value,
             )
     }

             val cardShimmerTranslateAnimation = transition(
                 definition = cardAnimationDefinition.shimmerTransitionDefinition,
                 initState = AnimationState.START,
                 toState = AnimationState.END
             )

             val colors = listOf(
                 MaterialTheme.colors.background.copy(0.9f),
                 MaterialTheme.colors.background.copy(0.1f),
                 MaterialTheme.colors.background.copy(0.9f))

             val xCardShimmer =
                 cardShimmerTranslateAnimation[cardAnimationDefinition.xShimmerPropKey]

             val yCardShimmer =
                 cardShimmerTranslateAnimation[cardAnimationDefinition.yShimmerPropKey]

         ScrollableColumn() {
             repeat(20){
                 ShimmerListCardItem(colors = colors,
                     rowHeight = rowHeight,
                     xShimmer = xCardShimmer,
                     yShimmer = yCardShimmer,
                     gradientWidth = cardAnimationDefinition.gradientWidth)
                 Divider(thickness = 0.5.dp)
             }
         }

         }
     }

package com.anappsdeveloper.ispend.presentation.ui.home

import android.os.Build
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.anappsdeveloper.ispend.data.ExpensesCategory
import com.anappsdeveloper.ispend.data.IncomeCategory
import com.anappsdeveloper.ispend.domain.ISpend
import com.anappsdeveloper.ispend.presentation.MyApp
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

@ExperimentalMaterialApi
@Composable
fun HeadHome(
    modifier: Modifier = Modifier,
    onAddItem: (ISpend) -> Unit,
    homeViewModel: HomeViewModel,
    application:MyApp
) {
        Surface(
            shape = CutCornerShape(15.dp)
                .copy(
                    topLeft = ZeroCornerSize,
                    topRight = ZeroCornerSize,
                    bottomRight = ZeroCornerSize
                )
       ) {
            SpendItemEntryInput(onAddItem = onAddItem, homeViewModel,modifier,application)
        }
}

@ExperimentalMaterialApi
@Composable
fun SpendItemEntryInput(onAddItem: (ISpend) -> Unit,
                        homeViewModel: HomeViewModel,
                        modifier: Modifier = Modifier,
                        application:MyApp) {
    Column(modifier = modifier) {

        val item = homeViewModel.item
        val price = homeViewModel.price
        val selectedCategory =  if(homeViewModel.expensesMode) homeViewModel.selectedExpensesCat.value else homeViewModel.selectedIncomeCat.value
        val onSelectedCategoryChanged = homeViewModel::onSelectedCategoryChanged
        val btnVisible = item.isNotBlank()

        /*add data in iSpendList*/
        val submit = {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                onAddItem(
                    ISpend(
                        item,
                        price,
                        selectedCategory,
                        LocalDate.now().toString(),
                        homeViewModel.expensesMode
                    )
                )
            } else {
                onAddItem(
                    ISpend(
                        item,
                        price,
                        selectedCategory,
                        SimpleDateFormat("yyyy-MM-dd").format(Date()),
                        homeViewModel.expensesMode
                    )
                )
            }

            homeViewModel.onItemChanged("")
            homeViewModel.onPriceChanged(0)
            if(homeViewModel.expensesMode)
            homeViewModel.onSelectedCategoryChanged(ExpensesCategory.MISC.value)
            else
                homeViewModel.onSelectedCategoryChanged(IncomeCategory.OTHER.value)
        }

        SpendItemInput(
            item = item,
            onItemChange = { homeViewModel.onItemChanged(it) },
            price = price.toString(),
            onPriceChange = { homeViewModel.onPriceChanged(it.toInt()) },
            selectedCategory = selectedCategory,
            onSelectedCategoryChanged = onSelectedCategoryChanged,
            /** val (name, age) = person ; val name = person.component1() val age = person.component2()*/
            submit = submit,
            btnEnable = btnVisible,
            homeViewModel = homeViewModel,
            application = application
        ) {
            EditButton(
                onClick = submit,
                text = "ADD",
                enabled = btnVisible
            )
        }
    }
}


@ExperimentalMaterialApi
@Composable
fun SpendItemInput(
    /** For add and edit*/

    item: String,
    onItemChange: (String) -> Unit,
    price: String,
    onPriceChange: (Int) -> Unit,
    selectedCategory: String,
    onSelectedCategoryChanged: (String) -> Unit,
    submit: () -> Unit,
    btnEnable: Boolean,
    homeViewModel: HomeViewModel,
    application: MyApp,
    buttonSlot: @Composable() () -> Unit
) {
    Row(modifier = Modifier.fillMaxWidth()) {

        OutlinedTextField(
            value = item,
            onValueChange = {
                if(it.length<=50)
                onItemChange(it) },
            textStyle = MaterialTheme.typography.body2.copy(color = MaterialTheme.colors.onSurface),
            label = { Text(text = "Item",style = MaterialTheme.typography.h5) },
            modifier = Modifier.weight(0.7f),
            placeholder = { Text(text = "e.g: Beer",style = MaterialTheme.typography.subtitle2) },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                autoCorrect = true,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            ),
            onImeActionPerformed = { action, softKeyboardController ->
                if (action == ImeAction.Next) {
                    softKeyboardController?.hideSoftwareKeyboard()
                }
            }
        )
        Spacer(modifier = Modifier.size(16.dp))
        OutlinedTextField(
            value = price,
            onValueChange = {
                if (it == "" || it.length > 7)
                    onPriceChange(0)
                else
                    onPriceChange(it.toInt())
            },
            textStyle = MaterialTheme.typography.body2.copy(color = MaterialTheme.colors.onSurface),
            label = { Text(text = "Price",style = MaterialTheme.typography.h5)},
            modifier = Modifier.weight(0.3f),
            placeholder = { Text(text = "e.g: 300",style = MaterialTheme.typography.subtitle2) },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Next
            ),
            onImeActionPerformed = { action, softKeyboardController ->
                if (action == ImeAction.Next) {
                    /* submit()*/
                    softKeyboardController?.hideSoftwareKeyboard()
                }
            }
        )
    }

    Spacer(modifier = Modifier.size(8.dp))

    CatChipSet(
        selectedCategory = selectedCategory,
        filterScrollPosition = homeViewModel.filterScrollPosition,
        onSelectedCategoryChanged = onSelectedCategoryChanged,
        homeViewModel = homeViewModel,
        application = application
    )

    Spacer(modifier = Modifier.size(16.dp))

    Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.SpaceBetween){
        IncomeExpensesRow(application,homeViewModel)

        Surface(content = buttonSlot, color = Color.Transparent)
    }

}
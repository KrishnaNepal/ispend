package com.anappsdeveloper.ispend.presentation.component


const val INCOME_MODE = "Income Mode"
const val EXPENSES_MODE = "Expenses Mode"
const val DAY_MODE = "Day Mode"
const val NIGHT_MODE = "Night Mode"

package com.anappsdeveloper.ispend.presentation.component

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.anappsdeveloper.ispend.R


class WebViewFragment : Fragment() {

    val fbURL = "https://www.facebook.com/About-App-109821537481694"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webview, container, false)
            .apply {
                findViewById<ComposeView>(R.id.compose_view).setContent {

               /**    val mAdView = findViewById<AdView>(R.id.adView)
                    val adRequest = AdRequest.Builder().build()
                    mAdView.loadAd(adRequest)*/
                       /* AndroidViewBinding(bindingBlock = FragmentWebviewBinding::inflate) {*/

                    /*val binding = FragmentWebviewBinding.inflate(layoutInflater)*/
                    val webView = findViewById<WebView>(R.id.web_view)
                    val progressBar = findViewById<ProgressBar>(R.id.progress_bar)

                            val webSettings = webView.settings
                            webSettings.javaScriptEnabled = true

                    webView.webViewClient = object : WebViewClient() {
                                override fun shouldOverrideUrlLoading(
                                    view: WebView,
                                    url: String
                                ): Boolean {
                                    view.loadUrl(url)
                                    return true
                                }

                                override fun onPageFinished(view: WebView, url: String) {
                                    progressBar.visibility = View.GONE
                                }

                                override fun onReceivedError(
                                    view: WebView,
                                    errorCode: Int,
                                    description: String,
                                    failingUrl: String
                                ) {
                                    progressBar.visibility = View.GONE
                                    Toast.makeText(
                                        activity,
                                        "Oh no! $description",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                    webView.loadUrl(fbURL)
                      /*  }*/
                        /*CircularProgressBarIndeterminate(isDisplay = loading, modifier = Modifier.padding(8.dp))*/
                        /*  AndroidView(viewBlock = ::WebView) { webView ->
                        with(webView){
                            settings.javaScriptEnabled = true
                            webViewClient = WebViewClient()
                            loadUrl(fbURL)
                        }
                    }*/
                    }
                }
            }
    }
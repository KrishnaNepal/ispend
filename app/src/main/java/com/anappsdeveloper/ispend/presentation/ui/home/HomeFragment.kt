package com.anappsdeveloper.ispend.presentation.ui.home

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.anappsdeveloper.ispend.R
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.ConnectivityMonitor
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import com.anappsdeveloper.ispend.presentation.theme.ISpendTheme
import com.anappsdeveloper.ispend.util.network.ConnectionLiveData
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalMaterialApi
@AndroidEntryPoint
class HomeFragment : Fragment() {

    @Inject
    lateinit var application: MyApp

    private val homeViewModel: HomeViewModel by viewModels()
    private val snackbarController = SnackbarController(lifecycleScope)

    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent {

                connectionLiveData = ConnectionLiveData(this@HomeFragment.requireContext())
                val isNetworkAvailable = connectionLiveData.observeAsState(false).value

                val navController = Navigation.findNavController(this)
                val mainPadding = Modifier.padding(top = 8.dp, start = 8.dp, end = 8.dp)
                val contentPadding = Modifier.padding(vertical = 8.dp, horizontal = 8.dp)

                val scaffoldState = rememberScaffoldState()

                ISpendTheme(darkTheme = application.isDark.value,
                    scaffoldState = scaffoldState,
                application = application,
                view = this) {
                    Scaffold(
                        topBar = {
                            TopBar(
                                title = stringResource(id = R.string.app_name),
                                viewModel = homeViewModel,
                                navController = navController,
                                snackbarController = snackbarController,
                                scaffoldState = scaffoldState,
                                application = application,
                            )
                        },
                      /*  bottomBar = {},
                        drawerContent = {},*/
                        scaffoldState = scaffoldState,
                        snackbarHost = { scaffoldState.snackbarHostState }
                    ) {

                            Column(modifier = mainPadding
                                .background(color = MaterialTheme.colors.background)) {

                                /** Check Network*/
                                    ConnectivityMonitor(isNetworkAvaialble = isNetworkAvailable)

                                HeadHome(
                                    onAddItem = homeViewModel::addItem,
                                    homeViewModel = homeViewModel,
                                    modifier = contentPadding,
                                    application = application
                                )

                                Spacer(modifier = Modifier.size(8.dp))

                                    BodyHome(
                                        itemList = homeViewModel.iSpendList,
                                        onEditItemChange = homeViewModel::onEditItemChange,
                                        onRemoveItem = homeViewModel::removeItem,
                                        snackbarController = snackbarController,
                                        scaffoldState = scaffoldState,
                                        homeViewModel = homeViewModel,
                                        application = application
                                    )
                                }
                        }
                    }
                }
            }
        }
    }
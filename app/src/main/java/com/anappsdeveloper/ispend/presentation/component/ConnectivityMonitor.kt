package com.anappsdeveloper.ispend.presentation.component

import android.os.Handler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun ConnectivityMonitor(isNetworkAvaialble:Boolean){
    var connectionString = ""
    if(!isNetworkAvaialble){
        connectionString = "No Internet"
        Text(text = connectionString,
            textAlign = TextAlign.Center,
            color = MaterialTheme.colors.error,
            style = MaterialTheme.typography.h5,
        modifier = Modifier.fillMaxWidth()
            .background(MaterialTheme.colors.background)
            .padding(bottom = 8.dp))
    }
}
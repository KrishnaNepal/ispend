package com.anappsdeveloper.ispend.presentation.component.shimmer

import androidx.compose.animation.core.*
import com.anappsdeveloper.ispend.presentation.component.shimmer.ShimmerAnimationDefinitions.AnimationState.*

class ShimmerAnimationDefinitions(
    private val widthPx: Float, /**screen width*/
    private val heightPx: Float, /**row height*/
) {
      var gradientWidth: Float = 0.2f * widthPx

    enum class AnimationState{
        START, END
    }

     val xShimmerPropKey = FloatPropKey("xShimmer")
     val yShimmerPropKey = FloatPropKey("yShimmer")

    val shimmerTransitionDefinition = transitionDefinition<AnimationState> {
        state(START){
            this[xShimmerPropKey] = 0f  /**both start from 0,0 cordinate*/
            this[yShimmerPropKey] = 0f
        }
        state(END){
            this[xShimmerPropKey] = widthPx + gradientWidth  //animation run after end of screen
                this[yShimmerPropKey] = heightPx /*+ gradientWidth*/
        }

        transition(START,END){
            xShimmerPropKey using infiniteRepeatable(
                animation = tween(
                    durationMillis = 1300,
                    delayMillis = 300,
                    easing = LinearEasing
                )
            )

            yShimmerPropKey using infiniteRepeatable(
                animation = tween(
                    durationMillis = 1300,
                    delayMillis = 300,
                    easing = LinearEasing
                )
            )
        }
    }
}
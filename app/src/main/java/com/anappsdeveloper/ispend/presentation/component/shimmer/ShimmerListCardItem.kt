package com.anappsdeveloper.ispend.presentation.component.shimmer

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.preferredHeight
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.unit.Dp

@Composable
fun ShimmerListCardItem(
    colors:List<Color>,
    rowHeight:Dp,
    xShimmer: Float,
    yShimmer: Float,
    gradientWidth:Float
    ){
    val brush = Brush.horizontalGradient(
        colors,
        startX = xShimmer - gradientWidth, //run animation before start screen
    endX = xShimmer,
        tileMode = TileMode.Clamp
       /* start = Offset(200f,200f),  //put default
    end = Offset(400f,400f)*/
    )
    Surface(shape = MaterialTheme.shapes.small) {
     Spacer(modifier = Modifier.fillMaxWidth()
         .preferredHeight(rowHeight)
         .background(brush = brush))
    }
}
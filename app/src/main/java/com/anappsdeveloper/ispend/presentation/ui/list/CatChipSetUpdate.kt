package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.foundation.ScrollableRow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import com.anappsdeveloper.ispend.data.ExpensesCategory
import com.anappsdeveloper.ispend.data.IncomeCategory
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.CategoryChip

@Composable
fun CatChipSetUpdate(
    selectedCategory: MutableState<String>,
    application: MyApp,
    expensesMode:Boolean
) {
    ScrollableRow(
    ) {
        if(expensesMode){
            for (category in ExpensesCategory.values()) {
                CategoryChip(
                    category = category.value,
                    isSelected = selectedCategory.value == category.value,
                    onSelectedCategoryChanged = { selectedCategory.value = it }
                ) {}
            }
        }else {
            for (category in IncomeCategory.values()) {
                CategoryChip(
                    category = category.value,
                    isSelected = selectedCategory.value == category.value,
                    onSelectedCategoryChanged = { selectedCategory.value = it }
                ) {}
            }
        }
    }
}
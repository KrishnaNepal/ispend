package com.anappsdeveloper.ispend.presentation

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApp : Application(){

    /** should be saved in datastore or cache*/

    val isDark = mutableStateOf(false)
    var isContactDialogDisplay by mutableStateOf(false)

    fun toggleLightTheme(){
        isDark.value = !isDark.value
    }
}
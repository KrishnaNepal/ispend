package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.anappsdeveloper.ispend.domain.ISpend
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.ListEmptyMsg
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import com.anappsdeveloper.ispend.presentation.theme.QuickSand
import com.anappsdeveloper.ispend.presentation.theme.shapes

@ExperimentalMaterialApi
@Composable
fun BodyHome(
    itemList: List<ISpend>,
    onEditItemChange: (ISpend, Int) -> Unit,
    onRemoveItem: (ISpend) -> Unit,
    scaffoldState: ScaffoldState,
    snackbarController: SnackbarController,
    homeViewModel: HomeViewModel,
    application: MyApp
) {
    if (itemList.isNullOrEmpty())
        ListEmptyMsg(msg = "List is empty!")
    else {
        var count = 0
   for (item in itemList) {
       count += item.price
   }
        Row(modifier = Modifier.fillMaxWidth().padding(bottom = 8.dp),horizontalArrangement = Arrangement.End){
            Text(
                text = "Total: ",
                color = MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
            Text(
                text =String.format("%,d", count),
                color = MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
        }


        CardListItem(item = itemList) { iSpend ->

            var item =  mutableStateOf(iSpend.item)
            var price =  mutableStateOf(iSpend.price)
            var cat = mutableStateOf(iSpend.cat)
            var isSwipe =  remember { mutableStateOf(false)}

            homeViewModel.iseditMode = isSwipe.value

            SwipeItemForDismissHome(isSwipe = isSwipe) {
                Card(
                    modifier = Modifier.fillMaxWidth(),
                    backgroundColor = if (isSwipe.value ) MaterialTheme.colors.surface.copy(0.6f) else MaterialTheme.colors.surface.copy(.9f),
                    shape = shapes.small,
                ) {
                    Row(modifier = Modifier.fillMaxWidth().padding(8.dp)) {

                        /** Cat */
                        BasicTextField(
                            value = cat.value,
                            onValueChange = {
                                    cat.value = it
                            },
                            enabled = false,
                            modifier = Modifier
                                .weight(0.2f)
                                .align(Alignment.CenterVertically),
                            textStyle = MaterialTheme.typography.subtitle2.copy(color =  MaterialTheme.colors.onSurface),
                            maxLines = 1,
                            keyboardOptions = KeyboardOptions(
                                autoCorrect = true,
                                keyboardType = KeyboardType.Text,
                                imeAction = ImeAction.Next
                            ),
                        )

                        Spacer(modifier = Modifier.size(8.dp))

                        /** Item */
                        BasicTextField(
                               value = item.value,
                           onValueChange = {
                               if(it.length<=50)
                                  item.value = it
                           },
                           enabled = isSwipe.value,
                            modifier = Modifier
                                .weight(0.6f)
                                .align(Alignment.CenterVertically),
                            textStyle = MaterialTheme.typography.body2.copy(color =  MaterialTheme.colors.onSurface),
                            singleLine = true,
                              keyboardOptions = KeyboardOptions(
                               autoCorrect = true,
                               keyboardType = KeyboardType.Text,
                               imeAction = ImeAction.Next
                           ),
                        )

                        Spacer(modifier = Modifier.size(4.dp))

                        /** Price */
                        BasicTextField(
                            value = price.value.toString(),
                            onValueChange = {
                                if (it == "" || it.length > 7)
                                    price.value = 0
                                else
                                    price.value = it.toInt()
                            },
                            enabled = isSwipe.value,
                            modifier = Modifier.weight(0.2f)
                                .align(Alignment.CenterVertically),
                            textStyle = MaterialTheme.typography.body1.copy(color =  MaterialTheme.colors.onSurface)
                            .copy(textAlign = TextAlign.End),
                            maxLines = 1,
                            keyboardOptions = KeyboardOptions(
                                autoCorrect = false,
                                keyboardType = KeyboardType.NumberPassword,
                                imeAction = ImeAction.Next
                            )
                        )
                    }
                }
            }
            Divider(modifier = Modifier.size(0.5.dp))
            SwipeContainerHome(isSwipe = isSwipe, deleteRow = {onRemoveItem(iSpend)} ,
                updateRow = {onEditItemChange(ISpend(item.value,price.value,cat.value,iSpend.date,iSpend.isExpenses),itemList.indexOf(iSpend))},
            scaffoldState = scaffoldState,
            snackbarController = snackbarController,
                cat = cat,
                application = application,
                homeViewModel = homeViewModel
            )

            Spacer(modifier = Modifier.size(8.dp))
        }
    }
}
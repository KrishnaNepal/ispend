package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState

@ExperimentalMaterialApi
@Composable
fun SwipeItemForDismissHome(isSwipe: MutableState<Boolean>, content: @Composable() () -> Unit) {

    val dismissState = rememberDismissState(
        confirmStateChange = {
            if (it == DismissValue.DismissedToStart) isSwipe.value = !isSwipe.value
            it != DismissValue.DismissedToStart
        }
    )

    SwipeToDismiss(state = dismissState,
        background = {},
        directions = setOf(DismissDirection.EndToStart),
        dismissContent = {
            content()
        })
}
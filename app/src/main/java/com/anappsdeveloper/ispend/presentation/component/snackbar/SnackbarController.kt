package com.anappsdeveloper.ispend.presentation.component.snackbar

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ScaffoldState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
class SnackbarController (private val scope: CoroutineScope){

    private var snackbarJob: Job? = null

    fun getScope() = scope

    fun showSnackbar(
        scaffoldState: ScaffoldState,
        message: String,
        actionLabel: String
    ){
        if(snackbarJob == null){
            showNewSnackbar(scaffoldState,message,actionLabel)
        }else{
            cancelActiveJob()
            showNewSnackbar(scaffoldState,message,actionLabel)
        }
    }

    private fun showNewSnackbar(
        scaffoldState: ScaffoldState,
        message: String,
        actionLabel: String
    ) {
        snackbarJob = scope.launch {
            scaffoldState.snackbarHostState.showSnackbar(
                message = message,
                actionLabel = actionLabel
            )
        }
    }

    private fun cancelActiveJob(){
        snackbarJob?.let{
            it.cancel() /** currently running cancel*/
            snackbarJob = Job() /** create new one*/
        }
    }
}
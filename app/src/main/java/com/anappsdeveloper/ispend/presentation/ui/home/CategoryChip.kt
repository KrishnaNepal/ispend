package com.anappsdeveloper.ispend.presentation.component

import android.graphics.Paint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun CategoryChip(
    category: String,
    isSelected: Boolean,
    onSelectedCategoryChanged:(String) -> Unit,
    onExecuteFilter: () -> Unit
) {
    Surface(
        modifier = Modifier.padding(end = 16.dp)
            .clickable(
                onClick = {
                    onSelectedCategoryChanged(category)
                    onExecuteFilter()
                }/*,enabled = !isSelected*/
            ),
        color = if(isSelected) MaterialTheme.colors.primary.copy(0.7f)  else MaterialTheme.colors.primary.copy(.1f),
        shape = MaterialTheme.shapes.small
    ) {
        Text(
            text = category,
            modifier = Modifier.padding(vertical = 4.dp,horizontal = 8.dp),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h4
        )
    }
}
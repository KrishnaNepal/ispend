package com.anappsdeveloper.ispend.presentation.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun CircularProgressBarIndeterminate(isDisplay:Boolean, modifier: Modifier= Modifier){
    if(isDisplay){
        Row(modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically) {
            CircularProgressIndicator()
        }
    }
}
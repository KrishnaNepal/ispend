package com.anappsdeveloper.ispend.presentation.ui.home

import android.graphics.Color
import android.view.Window
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.anappsdeveloper.ispend.R
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import com.anappsdeveloper.ispend.presentation.theme.QuickSand
import kotlinx.coroutines.launch
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

@ExperimentalMaterialApi
@Composable
fun TopBar(
    title: String,
    viewModel: HomeViewModel,
    navController: NavController,
    scaffoldState: ScaffoldState,
    snackbarController: SnackbarController,
    application:MyApp,
){

    val isSaveDisable = viewModel.iSpendList.isEmpty() || viewModel.iseditMode

    TopAppBar(backgroundColor = MaterialTheme.colors.primary){

            Row(Modifier.fillMaxWidth().fillMaxHeight().padding(vertical = 8.dp),horizontalArrangement = Arrangement.End) {
                IconButton(onClick = { navController.navigate(R.id.toListFragment) }) {
                    Icon(
                        Icons.Default.List,
                        tint = MaterialTheme.colors.onPrimary)
                }
                Spacer(Modifier.size(8.dp))
                IconButton(onClick =  {viewModel.insertDB()
                    snackbarController.getScope().launch { snackbarController
                        .showSnackbar(
                            scaffoldState = scaffoldState,
                            message = "Save succeeded",
                            actionLabel = "Hide"
                        )
                    }},
                    enabled = !isSaveDisable
                ){
                    Icon(
                        Icons.Default.Save,
                        tint = if(isSaveDisable) MaterialTheme.colors.onPrimary.copy(0.1f) else MaterialTheme.colors.onPrimary)
                }

                IconButton(onClick =  {application.isContactDialogDisplay = true}) {
                    Icon(Icons.Default.MoreVert)
                }

            }
    }
}
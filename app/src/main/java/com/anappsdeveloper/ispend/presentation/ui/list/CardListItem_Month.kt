package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun <T> CardListItem_Month(
    item: List<T>,
    listItem: @Composable (T, Int) -> Unit
) {
    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        itemsIndexed(items = item){ index, item ->
            listItem(item,index)
        }
    }
}
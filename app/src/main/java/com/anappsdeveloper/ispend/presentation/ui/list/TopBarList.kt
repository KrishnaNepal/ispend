package com.anappsdeveloper.ispend.presentation.ui.list

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Sort
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.anappsdeveloper.ispend.data.DateCategory
import com.anappsdeveloper.ispend.presentation.MyApp

@Composable
fun TopBarList(query: String,
               onQueryChanged: (String) -> Unit,
               onExecuteSearch: () -> Unit,
               onListOrderChanged: () -> Unit,
               onSearchClear: () -> Unit,
               isSearchShown:MutableState<Boolean>,
               application: MyApp,
               viewModel: ListViewModel) {

    Column(modifier = Modifier.fillMaxWidth().background(MaterialTheme.colors.primary)) {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {

                //Search Field
                if (isSearchShown.value && viewModel.selectedCategory == DateCategory.ALL)
                    TextField(value = query,
                        onValueChange = {
                            if (it.length <= 30)
                                onQueryChanged(it)
                        },
                        textStyle = MaterialTheme.typography.body2.copy(color = MaterialTheme.colors.onPrimary),
                        activeColor = MaterialTheme.colors.onPrimary,
                        enabled = viewModel.selectedCategory == DateCategory.ALL,
                        modifier = Modifier.weight(0.7f),
                        label = {
                            Text(
                                text = if (viewModel.selectedCategory == DateCategory.ALL) "Search" else "Search Disabled",
                                  color = MaterialTheme.colors.onPrimary,
                                style = MaterialTheme.typography.h5
                            )
                        },
                        singleLine = true,
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Text,
                            imeAction = ImeAction.Search
                        ),
                        leadingIcon = {
                            Icon(
                                Icons.Filled.Search, modifier = Modifier
                                    .clickable(onClick = {onExecuteSearch()})
                                ,tint = MaterialTheme.colors.onPrimary
                            )
                        },
                        trailingIcon = {
                            Icon(
                                Icons.Filled.Close,
                                modifier = Modifier
                                    .clickable(onClick = { onSearchClear() }),
                                tint = MaterialTheme.colors.onPrimary
                            )
                        },
                        onImeActionPerformed = { imeAction, softwareKeyboardController ->
                            if (imeAction == ImeAction.Search) {
                                if (query.isNotEmpty()) {
                                    onExecuteSearch()
                                }
                                softwareKeyboardController?.hideSoftwareKeyboard()
                            }
                        }
                    )

                if (!isSearchShown.value && viewModel.selectedCategory == DateCategory.ALL)
                    IconButton(
                        onClick = { isSearchShown.value = true },
                    ) {
                        Icon(Icons.Filled.Search,tint = MaterialTheme.colors.onPrimary)
                    }

                IconButton(
                    onClick = onListOrderChanged,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                ) {
                    Icon(Icons.Filled.Sort,tint = MaterialTheme.colors.onPrimary)
                }

                IconButton(
                    onClick = { application.isContactDialogDisplay = true },
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                    /* .weight(0.1f)*/
                ) {
                    Icon(Icons.Default.MoreVert,tint = MaterialTheme.colors.onPrimary)
                }
            }

            DateChipSet(
                selectedCategory = viewModel.selectedCategory,
                onSelectedCategoryChanged = viewModel::onSelectedCategoryChanged,
                filterScrollPosition = viewModel.filterScrollPosition,
                onExecuteFilter = viewModel::fetchDayMonthFromDB,
                viewModel
            )
    }
}
package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anappsdeveloper.ispend.data.ExpensesCategory
import com.anappsdeveloper.ispend.data.IncomeCategory
import com.anappsdeveloper.ispend.data.getExpensesCategory
import com.anappsdeveloper.ispend.data.getIncomeCategory
import com.anappsdeveloper.ispend.domain.*
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.repository.ItemError
import com.anappsdeveloper.ispend.repository.MainRepository
import kotlinx.coroutines.launch

class HomeViewModel
@ViewModelInject
constructor(private val mainRepository: MainRepository, private val application:MyApp) : ViewModel() {

        var iSpendList by mutableStateOf(listOf<ISpend>())
        private set

    var iseditMode by mutableStateOf(false)

    var item by mutableStateOf("")
    private set

    var price by mutableStateOf(0)
    private set

    var expensesMode by mutableStateOf(true)

    var selectedExpensesCat by mutableStateOf(ExpensesCategory.MISC)
        private set

    var selectedIncomeCat by mutableStateOf(IncomeCategory.OTHER)
        private set

    var filterScrollPosition:Float = 1f

    fun onSelectedCategoryChanged(category:String){
        if(expensesMode){
            val newCategory = getExpensesCategory(category)
            if (newCategory != null) {
                selectedExpensesCat = newCategory
            }
        }else{
            val newCategory = getIncomeCategory(category)
            if (newCategory != null) {
                selectedIncomeCat = newCategory
            }
        }
    }

    fun addItem(ISpend: ISpend) {
        iSpendList += listOf(ISpend)
    }

    fun removeItem(ISpend:ISpend){
        iSpendList = iSpendList.toMutableList().also { it.remove(ISpend) }
    }

    fun onEditItemChange(ISpend:ISpend, currentEditPosition:Int){
        iSpendList = iSpendList.toMutableList().also { it[currentEditPosition] = ISpend }
    }

    fun onItemChanged(item:String){
        this.item = item
    }

    fun onPriceChanged(price:Int){
        this.price = price
    }

    fun expensesModeActive(){
        expensesMode = true
    }

    fun incomeModeActive(){
        expensesMode = false
    }


    fun insertDB() {
        launchDataLoad {
               mainRepository.insertMainEntity(iSpendList)
        }
    }

    private fun launchDataLoad(block: suspend () -> Unit): Unit {
        viewModelScope.launch {
            try {
                block()
                iSpendList = emptyList()
            } catch (error: ItemError) {
            } finally {
            }
        }
    }
}
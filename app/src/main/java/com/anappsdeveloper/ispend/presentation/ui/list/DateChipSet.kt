package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.anappsdeveloper.ispend.data.DateCategory
import com.anappsdeveloper.ispend.presentation.component.CategoryChip
import java.sql.Date

@Composable
fun DateChipSet(
    selectedCategory: DateCategory,
    onSelectedCategoryChanged: (String) -> Unit,
    filterScrollPosition: Float,
    onExecuteFilter: () -> Unit,
    listViewModel: ListViewModel) {

        val scrollState = rememberScrollState()
        ScrollableRow(scrollState = scrollState,horizontalArrangement = Arrangement.SpaceBetween,modifier = Modifier.fillMaxWidth()/*.padding(bottom = 2.dp)*/) {

           scrollState.scrollTo(filterScrollPosition) //show respective position after rotate
            for (category in DateCategory.values()) {
                DateChip(
                    category = category.value,
                    isSelected = selectedCategory == category,
                    onSelectedCategoryChanged = {
                       onSelectedCategoryChanged(it)
                        listViewModel.filterScrollPosition = scrollState.value
                    },
                    onExecuteFilter = onExecuteFilter
                )
            }
        }
}
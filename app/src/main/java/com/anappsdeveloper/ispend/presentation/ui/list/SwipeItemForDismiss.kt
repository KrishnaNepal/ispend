package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState

@ExperimentalMaterialApi
@Composable
fun SwipeItemForDismissList(isSwipe: MutableState<Boolean>, content: @Composable() () -> Unit){
    val dismissState = rememberDismissState(
        confirmStateChange = {
            /**here All or Day,not swiped: then swipe work*/
            if (it == DismissValue.DismissedToStart  && !isSwipe.value) isSwipe.value = !isSwipe.value
            it != DismissValue.DismissedToStart
        }
    )

    SwipeToDismiss(state = dismissState,
        background =  {},
        directions = setOf(DismissDirection.EndToStart),
        dismissContent = {
            content()
        })
}
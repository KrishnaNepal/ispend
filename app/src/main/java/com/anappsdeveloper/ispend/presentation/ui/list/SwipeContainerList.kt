package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Save
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
fun SwipeContainerList(
    isSwipe: MutableState<Boolean>,
    deleteRow: () -> Unit,
    updateRow: () -> Unit,
    scaffoldState: ScaffoldState,
    snackbarController: SnackbarController,
    listViewModel: ListViewModel,
    cat: MutableState<String>,
    application: MyApp,
    expensesMode:Boolean
) {

    if (isSwipe.value)
        Column(modifier = Modifier.background(color = MaterialTheme.colors.surface)) {

            Row(modifier = Modifier.padding(start = 8.dp, end = 8.dp, top = 8.dp)) {
                CatChipSetUpdate(
                    selectedCategory = cat,
                    application = application, /**here selectcategory will be cat declared; as it is mutableState*/
                expensesMode = expensesMode
                )
            }

            Row(
                modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.End
            ) {
                val submit = {
                    updateRow()
                    isSwipe.value = false
                    snackbarController.getScope().launch {
                        snackbarController
                            .showSnackbar(
                                scaffoldState = scaffoldState,
                                message = "Update succeeded",
                                actionLabel = "Hide"
                            )
                    }
                }

                /** Close Icon */
                IconButton(
                    onClick = {
                        isSwipe.value = false
                    },
                    modifier = Modifier.align(Alignment.CenterVertically)
                ) {
                    Icon(Icons.Default.Close, tint = MaterialTheme.colors.onSurface)
                }

                Spacer(modifier = Modifier.size(8.dp))

                /** Delete Row */
                IconButton(
                    onClick = {
                        deleteRow()
                        isSwipe.value = false
                        snackbarController.getScope().launch {
                            snackbarController
                                .showSnackbar(
                                    scaffoldState = scaffoldState,
                                    message = "Delete succeeded",
                                    actionLabel = "Hide"
                                )
                        }
                    },
                    modifier = Modifier.align(Alignment.CenterVertically)
                ) {
                    Icon(Icons.Default.Delete, tint = MaterialTheme.colors.onSurface)
                }

                /** Update Row */
                Spacer(modifier = Modifier.size(8.dp))

                IconButton(
                    onClick = { submit() },
                    modifier = Modifier.align(Alignment.CenterVertically)
                ) {
                    Icon(Icons.Default.Save, tint = MaterialTheme.colors.onSurface)
                }
            }
        }
}

package com.anappsdeveloper.ispend.presentation.ui.home

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.anappsdeveloper.ispend.presentation.MyApp

@ExperimentalMaterialApi
@Composable
fun IncomeExpensesRow(application: MyApp, homeViewModel: HomeViewModel) {

    Row(){

        RadioButton(selected = homeViewModel.expensesMode, onClick = {homeViewModel.expensesModeActive()},
        colors = RadioButtonDefaults.colors(MaterialTheme.colors.error),
        modifier = Modifier.align(Alignment.CenterVertically))

        TextButton(onClick = {homeViewModel.expensesModeActive()},modifier = Modifier.align(Alignment.CenterVertically),
        enabled = !homeViewModel.expensesMode) {
            Text(text = "Expenses",
                style = MaterialTheme.typography.subtitle1
                ,color = MaterialTheme.colors.error
           )
        }

        Spacer(modifier = Modifier.size(8.dp))

        RadioButton(selected = !homeViewModel.expensesMode, onClick = {homeViewModel.incomeModeActive()},
            colors = RadioButtonDefaults.colors(MaterialTheme.colors.primaryVariant),
            modifier = Modifier.align(Alignment.CenterVertically))

        TextButton(onClick = {homeViewModel.incomeModeActive()}, modifier = Modifier.align(Alignment.CenterVertically),
            enabled = homeViewModel.expensesMode) {
            Text(text = "Income",
                style = MaterialTheme.typography.subtitle1,
                color = MaterialTheme.colors.primaryVariant)
        }
    }
}

@Composable
fun EditButton(
    onClick: () -> Unit,
    text: String,
    enabled: Boolean = true
){
    Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.End){
        Button(
            onClick = onClick,
            shape = MaterialTheme.shapes.small,
            enabled = enabled
        ) {
            Text(text = text,color = MaterialTheme.colors.onPrimary, style = MaterialTheme.typography.button)
        }
    }
}
package com.anappsdeveloper.ispend.presentation.component.snackbar

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@ExperimentalMaterialApi
@Composable
fun DefaultSnackbar(
    snackbarHostState: SnackbarHostState,
    modifier: Modifier = Modifier
){
    SnackbarHost(
        modifier = modifier,
        hostState = snackbarHostState,
        snackbar = {
            Snackbar(
                backgroundColor = MaterialTheme.colors.surface,
                contentColor = MaterialTheme.colors.onSurface,
                modifier = Modifier.padding(8.dp),
                text = { Text( text = it.message,style = MaterialTheme.typography.h5)},
                action = {
                    it.actionLabel?.let {
                        TextButton(onClick = { snackbarHostState.currentSnackbarData?.dismiss() }) {
                            Text( text = it,style = MaterialTheme.typography.button,color = MaterialTheme.colors.onSurface) }
                    }
                }
            )
        }
    )
}
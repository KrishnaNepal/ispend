package com.anappsdeveloper.ispend.presentation.theme

import androidx.compose.material.Typography
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.font
import androidx.compose.ui.text.font.fontFamily
import androidx.compose.ui.unit.sp
import com.anappsdeveloper.ispend.R

 val QuickSand = fontFamily(
        font(R.font.quicksand_light, FontWeight.W300),
        font(R.font.quicksand_regular, FontWeight.W400),
        font(R.font.quicksand_medium, FontWeight.W500),
        font(R.font.quicksand_bold, FontWeight.W600)
)

val QuickSandTypography = Typography(

        h2 = TextStyle( /*Date Category*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
        ),

        h4 = TextStyle( /*Msg: List is empty, Menu, CatChip*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp,
        ),
        h5 = TextStyle( /*label, No Internet, Snackbar*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.Light,
                fontSize = 15.sp,
        ),

        subtitle1 = TextStyle( /*Expenses, Saving, Total*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.SemiBold,
                fontSize = 14.sp,
        ),
        subtitle2 = TextStyle( /* Total, Income amount of list*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.SemiBold,
                fontSize = 12.sp,
        ),
        body1 = TextStyle( /*price*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp
        ),
        body2 = TextStyle( /*text, textfield*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.Normal,
                fontSize = 16.sp
        ),
        button = TextStyle( /*button*/
                fontFamily = QuickSand,
                fontWeight = FontWeight.SemiBold,
                fontSize = 16.sp,
        ),

        /* h1 = TextStyle(
               fontFamily = QuickSand,
               fontWeight = FontWeight.W500,
               fontSize = 30.sp,
       ),*/

        /* h3 = TextStyle(
               fontFamily = QuickSand,
               fontWeight = FontWeight.Light,
               fontSize = 15.sp,
       ),*/
        /* h6 = TextStyle(
               fontFamily = QuickSand,
               fontWeight = FontWeight.Light,
               fontSize = 13.sp,
       ),*/

       /* caption = TextStyle(
                fontFamily = QuickSand,
                fontWeight = FontWeight.Normal,
                fontSize = 12.sp
        ),
        overline = TextStyle(
                fontFamily = QuickSand,
                fontWeight = FontWeight.W400,
                fontSize = 12.sp
        )*/
)
package com.anappsdeveloper.ispend.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.CommunicationDialog
import com.anappsdeveloper.ispend.presentation.component.ConnectivityMonitor
import com.anappsdeveloper.ispend.presentation.component.snackbar.DefaultSnackbar
import com.anappsdeveloper.ispend.util.network.ConnectionLiveData
import javax.inject.Inject

private val LightColorPalette = lightColors(
    primary = Blue600, /**toolbar, button*/
    primaryVariant = Blue600, /**Income*/
    onPrimary = Grey50, /**toolbar ,button text*/
    error = Red900, /**Expenses*/
    background = Grey200,   /**background*/
    onBackground = Grey800, /**background text*/
    surface = Color.White, /**surface*/
    onSurface = Grey700 /**surface text*/

    /*secondary = Blue50,
       secondaryVariant = Grey1,*/
    /*  onSecondary = Blue900,*/
    /*onError = Color.White,*/
)

private val DarkColorPalette = darkColors(
    primary = Grey900,
    primaryVariant = Blue900,
    onPrimary =Grey700,
    error = Red900Dark,
    background = Color.Black,
    onBackground = Grey600,
    surface = Grey800,
    onSurface = Grey600

    /*  secondary = Grey500BlueGrey500,
       onSecondary = Grey300Grey500,*/
    /*onError = Grey500,*/
)

@ExperimentalMaterialApi
@Composable
fun ISpendTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    scaffoldState: ScaffoldState,
    application: MyApp,
    view: ComposeView,
    content: @Composable() () -> Unit,
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = QuickSandTypography,
        shapes = shapes,
        /* content = content*/
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            CommunicationDialog(application,view)
            content()
            DefaultSnackbar(
                snackbarHostState = scaffoldState.snackbarHostState,
                modifier = Modifier.align(Alignment.BottomCenter)
            )
        }
    }
}
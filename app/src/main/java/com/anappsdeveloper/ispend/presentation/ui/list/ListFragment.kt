package com.anappsdeveloper.ispend.presentation.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.material.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.ConnectivityMonitor
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import com.anappsdeveloper.ispend.presentation.theme.ISpendTheme
import com.anappsdeveloper.ispend.util.network.ConnectionLiveData
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalMaterialApi
@AndroidEntryPoint
class ListFragment:Fragment() {

    @Inject
    lateinit var application:MyApp
    private val listViewModel: ListViewModel by viewModels()
    private val snackbarController = SnackbarController(lifecycleScope)

    lateinit var connectionLiveData:ConnectionLiveData

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply { 
            setContent {
4
                connectionLiveData = ConnectionLiveData(this@ListFragment.requireContext())
                val isNetworkAvailable = connectionLiveData.observeAsState(false).value
                val scaffoldState = rememberScaffoldState()

                val mainPadding = Modifier.padding(top = 8.dp, start = 8.dp, end = 8.dp)
                val contentPadding = Modifier.padding(vertical = 8.dp,horizontal = 8.dp)

                ISpendTheme(darkTheme = application.isDark.value,
                    scaffoldState = scaffoldState,
                    application = application,
                view = this) {

                    Scaffold(
                        topBar = { TopBarList(
                            query = listViewModel.query,
                            onQueryChanged = listViewModel::onQueryChanged,
                            onExecuteSearch = listViewModel::newSearch,
                            onListOrderChanged = listViewModel::onListOrderChanged,
                            onSearchClear = listViewModel::clearSearch,
                            isSearchShown = listViewModel.isSearchShown,
                            application = application,
                            viewModel = listViewModel) },
                        /* drawerContent = {},
                        bottomBar = {},*/
                        scaffoldState = scaffoldState,
                        snackbarHost = { scaffoldState.snackbarHostState }
                    ) {
                            Column( modifier = mainPadding.background(MaterialTheme.colors.background)) {

                                /** Check Network */
                                ConnectivityMonitor(isNetworkAvaialble = isNetworkAvailable)

                                BodyList(
                                    dailyEntities = listViewModel.dailyEntities,
                                    monthlyEntities = listViewModel.monthlyEntities,
                                    allEntities = listViewModel.allLiveData.observeAsState(listOf()).value,
                                    updateMainEntity = listViewModel::updateMainEntityById,
                                    deleteMainEntity = listViewModel::deleteMainEntityById,
                                    snackbarController = snackbarController,
                                    scaffoldState = scaffoldState,
                                    listViewModel = listViewModel,
                                    loading = listViewModel.loading,
                                    context = this@ListFragment,
                                    application = application,
                                    totalExpenses = listViewModel.totalExpenses.observeAsState(0).value,
                                    totalIncome = listViewModel.totalIncome.observeAsState(0).value
                                )
                            }
                    }
                }
            }
        }
    }
}
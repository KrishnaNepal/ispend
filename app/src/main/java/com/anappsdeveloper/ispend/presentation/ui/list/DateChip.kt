package com.anappsdeveloper.ispend.presentation.ui.list

import android.graphics.fonts.FontStyle
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp

@Composable
fun DateChip(
    category: String,
    isSelected: Boolean,
    onSelectedCategoryChanged:(String) -> Unit,
    onExecuteFilter: () -> Unit,
) {

        Column(modifier = Modifier.clickable(onClick = {
            onSelectedCategoryChanged(category)
            onExecuteFilter()
        })) {
            ConstraintLayout {
                val (text, divider) = createRefs()
                Text(
                    text = category,
                    textAlign = TextAlign.Center,
                    color = if (isSelected) MaterialTheme.colors.onPrimary else MaterialTheme.colors.onPrimary.copy(
                        0.5f
                    ),
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.padding(vertical = 8.dp, horizontal = 16.dp)
                        .constrainAs(text) {
                            top.linkTo(parent.top)
                        }
                )

                Divider(modifier = Modifier
                    .width(100.dp)
                    .height(3.dp)
                   /* .background(color = if (isSelected) MaterialTheme.colors.onPrimary else MaterialTheme.colors.primary)*/
                    .constrainAs(divider) {
                        top.linkTo(text.bottom)
                        centerHorizontallyTo(parent)
                    },
                    color = if (isSelected) MaterialTheme.colors.onPrimary else MaterialTheme.colors.primary
                )
            }
        }
    }

package com.anappsdeveloper.ispend.presentation.component

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import androidx.navigation.Navigation
import com.anappsdeveloper.ispend.R
import com.anappsdeveloper.ispend.presentation.MyApp

@Composable
fun CommunicationDialog(application: MyApp, view: ComposeView) {
    if (application.isContactDialogDisplay)
        Popup(
            onDismissRequest = { application.isContactDialogDisplay = false },
            alignment = Alignment.TopEnd
        ) {
            Surface(
                elevation = 8.dp,
                color = MaterialTheme.colors.surface
            ) {
                Column(modifier = Modifier.padding(8.dp)) {

                        TextButton(onClick = {
                            application.toggleLightTheme()
                            application.isContactDialogDisplay = false
                        }) {
                            Text(
                                text = if (application.isDark.value) DAY_MODE else NIGHT_MODE,
                                color = MaterialTheme.colors.onSurface,
                                style = MaterialTheme.typography.h4
                            )
                        }

                    TextButton(onClick = {
                        val navController = Navigation.findNavController(view)
                        navController.navigate(R.id.to_webViewFragment)

                        application.isContactDialogDisplay = false
                    }) {
                        Text(
                            text = "Facebook Page",
                            color = MaterialTheme.colors.onSurface,
                            style = MaterialTheme.typography.h4
                        )
                    }

                    TextButton(onClick = {
                        share(view.context)
                        application.isContactDialogDisplay = false
                    }) {
                        Text(
                            text = "Share",
                            color = MaterialTheme.colors.onSurface,
                            style = MaterialTheme.typography.h4
                        )
                    }
                }
            }
        }
}

fun share(context: Context) {
    val subject = "APP: " + context.resources.getString(R.string.app_name)
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        var shareMessage = "\nLet me recommend you this application\n\n"
        shareMessage =
            """
            ${shareMessage}https://play.google.com/store/apps/details?id=${context.packageName}
            
            
            """.trimIndent()
        intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
        context.startActivity(Intent.createChooser(intent, "choose one"))
    } catch (e: Exception) {

    }
}

fun rateApp(application: MyApp) {
    try {
        val rateIntent = rateIntentForUrl("market://details", application)
        application.startActivity(rateIntent)
    } catch (e: ActivityNotFoundException) {
        val rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details", application)
        application.startActivity(rateIntent)
    }
}

private fun rateIntentForUrl(url: String, application: MyApp): Intent {
    val intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse(
            String.format(
                "%s?id=%s",
                url,
                application.packageName
            )
        )
    )
    var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
    flags = if (Build.VERSION.SDK_INT >= 21) {
        flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT
    } else {
        flags or Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
    }
    intent.addFlags(flags)
    return intent
}
package com.anappsdeveloper.ispend.presentation.component

import androidx.compose.foundation.Icon
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Save
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.navigation.Navigation.findNavController
import com.anappsdeveloper.ispend.presentation.ui.home.HomeViewModel
import com.anappsdeveloper.ispend.R
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.ui.home.HomeFragment

@Composable
fun DefaultTopBar(title:String, modifier:Modifier = Modifier){
    Surface(){
        TopAppBar(modifier.fillMaxWidth()){
            Row(modifier.padding(8.dp).fillMaxWidth().fillMaxHeight(),verticalAlignment = Alignment.CenterVertically) {
                Text(text = title)
            }
        }
    }
}
package com.anappsdeveloper.ispend.presentation.ui.list

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.anappsdeveloper.ispend.R
import com.anappsdeveloper.ispend.data.DateCategory.*
import com.anappsdeveloper.ispend.data.model.DateFormatModel
import com.anappsdeveloper.ispend.data.model.DateFormatMonthModel
import com.anappsdeveloper.ispend.data.model.DayMonthModel
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.presentation.component.CircularProgressBarIndeterminate
import com.anappsdeveloper.ispend.presentation.component.ListEmptyMsg
import com.anappsdeveloper.ispend.presentation.component.shimmer.LoadingListShimmer
import com.anappsdeveloper.ispend.presentation.component.snackbar.SnackbarController
import com.anappsdeveloper.ispend.presentation.theme.QuickSand
import com.anappsdeveloper.ispend.room.MainEntity
import java.util.*

@ExperimentalMaterialApi
@Composable
fun BodyList(
    dailyEntities: List<DayMonthModel>,
    monthlyEntities: List<DayMonthModel>,
    allEntities: List<MainEntity>,
    updateMainEntity: (MainEntity) -> Unit,
    deleteMainEntity: (MainEntity) -> Unit,
    scaffoldState: ScaffoldState,
    snackbarController: SnackbarController,
    listViewModel: ListViewModel,
    loading: MutableState<Boolean>,
    context: ListFragment,
    application: MyApp,
    totalExpenses: Int,
    totalIncome: Int
) =
    if (loading.value && allEntities.isEmpty()) //loading but data not arrived at
        LoadingListShimmer(rowHeight = 40.dp)
    else
        Box(modifier = Modifier.fillMaxSize()) {

            CircularProgressBarIndeterminate(
                isDisplay = loading.value,
                modifier = Modifier.align(Alignment.BottomCenter).padding(bottom = 8.dp)
            )

            if (allEntities.isNullOrEmpty()) //here allEntities will be empty while search not found
                ListEmptyMsg(msg = "List is empty!")
            else
                Column() {
                    Row(
                        modifier = Modifier.padding(vertical = 8.dp).fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {

                        ConstraintLayout{
                            val (title,subtitle) = createRefs()

                            Text(text = "Saving",
                                style = MaterialTheme.typography.subtitle1,
                                color = MaterialTheme.colors.onBackground,
                                modifier = Modifier.constrainAs(title){
                                    top.linkTo(parent.top)
                                })

                            Text(text = String.format("%,d", totalIncome - totalExpenses),
                                style = MaterialTheme.typography.subtitle2,
                                color = MaterialTheme.colors.onBackground,
                            modifier = Modifier.constrainAs(subtitle){
                                top.linkTo(title.bottom)
                                centerHorizontallyTo(parent)
                            })
                        }

                        ConstraintLayout{
                            val (title,subtitle) = createRefs()

                            Text(text = "Expenses",
                                style = MaterialTheme.typography.subtitle1,
                                color = MaterialTheme.colors.error,
                                modifier = Modifier.constrainAs(title){
                                    top.linkTo(parent.top)
                                })

                            Text(text = String.format("%,d", totalExpenses),
                                style = MaterialTheme.typography.subtitle2,
                                color = MaterialTheme.colors.error,
                                modifier = Modifier.constrainAs(subtitle){
                                    top.linkTo(title.bottom)
                                    centerHorizontallyTo(parent)
                                })
                        }

                        ConstraintLayout{
                            val (title,subtitle) = createRefs()

                            Text(text = "Income",
                                style = MaterialTheme.typography.subtitle1,
                                color = MaterialTheme.colors.primaryVariant,
                                modifier = Modifier.constrainAs(title){
                                    top.linkTo(parent.top)
                                })

                            Text(text = String.format("%,d", totalIncome),
                                style = MaterialTheme.typography.subtitle2,
                                color = MaterialTheme.colors.primaryVariant,
                                modifier = Modifier.constrainAs(subtitle){
                                    top.linkTo(title.bottom)
                                    centerHorizontallyTo(parent)
                                })
                        }

                    }
                    Divider(thickness = 0.5.dp)
                    Spacer(modifier = Modifier.size(8.dp))

                    when (listViewModel.selectedCategory) {
                        ALL -> {
                            CardListItem_Index(
                                viewModel = listViewModel,
                                items = allEntities
                            ) { allEntity: MainEntity, index ->
                                var item = mutableStateOf(allEntity.item)
                                var price = mutableStateOf(allEntity.price)
                                var date = mutableStateOf(allEntity.date)
                                var isSwipe = remember { mutableStateOf(false) }
                                /** remember intentionally*/
                                /**here isSwipe is remember if not then while recompose isSwipe become always false
                                 * as it is inside function. remember stores and helps to get perv value while recompose*/
                                /** var item is mutablestateOf as all place where we put item get update when change occured*/

                                var cat = mutableStateOf(allEntity.cat)

                                /** Swipe Item */
                                allField(item,
                                    price,
                                    date,
                                    cat,
                                    isSwipe,
                                    listViewModel,
                                    application,
                                    context,
                                    allEntity.isExpenses,
                                    index,
                                    allEntities,
                                    scaffoldState,
                                    snackbarController,
                                    { deleteMainEntity(allEntity) }
                                ) {
                                    updateMainEntity(
                                        MainEntity(
                                            allEntity.id,
                                            item.value,
                                            price.value,
                                            cat.value,
                                            date.value,
                                            allEntity.isExpenses
                                        )
                                    )
                                }
                            }
                        }

                        DAILY -> {
                            CardListItem_Index(
                                viewModel = listViewModel,
                                items = dailyEntities
                            ) { item, index ->
                                dayMonthField(item, index, dailyEntities,listViewModel,true)
                            }
                        }

                        MONTHLY -> {
                            CardListItem_Month(item = monthlyEntities) { item, index ->
                                dayMonthField(item, index, monthlyEntities,listViewModel,false)
                            }
                        }
                    }
                }
        }

var count: Int = -1

@Composable
fun dayMonthField(item: DayMonthModel, index: Int, dayMonthEntities: List<DayMonthModel>,
                  listViewModel: ListViewModel,isDaily:Boolean) {
    if (index != count)
    /** ignore item. same DATE data already append*/
    {
        Row(
            modifier = Modifier.fillMaxWidth()
                .padding(
                    vertical = 12.dp
                ), horizontalArrangement = Arrangement.SpaceBetween
        ) {
            /** stop before arriving last index | current index date == next index date*/
            if (index < dayMonthEntities.size - 1 && dayMonthEntities[index].date == dayMonthEntities[index + 1].date) {
                count = index + 1
                /** date repeated, have to ignore index+1 item*/
                if(isDaily)
                groupDateFormat(changeDateFormat = listViewModel.changeDateFormat(item.date))
                else
                    groupDateFormatMonth(changeDateFormat = listViewModel.changeDateFormatMonth(item.date))

                Text(
                    /**if income append next index sum; as date is repeated*/
                    text = if (item.isExpenses) item.sum.toString() else dayMonthEntities[index + 1].sum.toString(),
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.body1
                )

                Text(
                    /**if expenses append next index sum; as date is repeated*/
                    text = if (!item.isExpenses) item.sum.toString() else dayMonthEntities[index + 1].sum.toString(),
                    color = MaterialTheme.colors.primaryVariant,
                    style = MaterialTheme.typography.body1
                )

            } else {
                if (isDaily)
                groupDateFormat(changeDateFormat = listViewModel.changeDateFormat(item.date))
                else
                    groupDateFormatMonth(changeDateFormat = listViewModel.changeDateFormatMonth(item.date))

                Text(
                    text = if (item.isExpenses) item.sum.toString() else "0",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.body1
                )

                Text(
                    text = if (!item.isExpenses) item.sum.toString() else "0",
                    color = MaterialTheme.colors.primaryVariant,
                    style = MaterialTheme.typography.body1
                )
            }
        }


        /* if (index != count)*/
        Divider(thickness = 0.5.dp)
    }
}

var allSeparate = true

@Composable
@ExperimentalMaterialApi
fun allField(
    item: MutableState<String>,
    price: MutableState<Int>,
    date: MutableState<String>,
    cat: MutableState<String>,
    isSwipe: MutableState<Boolean>,
    listViewModel: ListViewModel,
    application: MyApp,
    context: ListFragment,
    expensesMode: Boolean,
    index: Int,
    allEntities: List<MainEntity>,
    scaffoldState: ScaffoldState,
    snackbarController: SnackbarController,
    deleteRow: () -> Unit,
    updateRow: () -> Unit,
) {

    /**For Grouping same Date*/
    if (index < allEntities.size - 1 && allEntities[index].date == allEntities[index + 1].date) {
        if (allSeparate)
        /**First countIndex = true*/
            groupDateFormat(listViewModel.changeDateFormat(date.value))

        allSeparate = false
    } else {
        if (allSeparate)
        /**First countIndex = true | if equal date(false) then unequal date*/
            groupDateFormat(listViewModel.changeDateFormat(date.value))
        allSeparate = true
    }

    SwipeItemForDismissList(isSwipe = isSwipe) {

        Row(
            modifier = Modifier.fillMaxWidth()
                /** if swipe color change else color unchange*/
                .background(color = if (isSwipe.value) MaterialTheme.colors.surface else MaterialTheme.colors.background)
                .padding(
                    vertical = 8.dp,
                    horizontal = if (isSwipe.value) 8.dp else 0.dp
                ),
        ) {

            /** Date TextButton */
            if (isSwipe.value)
                TextButton(enabled = isSwipe.value,
                    colors = ButtonDefaults.buttonColors(),
                    onClick = {
                        val str = StringTokenizer(date.value, "-")

                        val year = str.nextToken()
                        var month = str.nextToken()
                        var day = str.nextToken()

                        val datePickerDialog = DatePickerDialog(
                            /** don't put here; as if put in loop fetch data will be slow*/
                            context.requireActivity(),DatePickerDialog.OnDateSetListener
                            { datePicker: DatePicker, year: Int, month: Int, day: Int ->
                                date.value = "$year-${listViewModel.addZero(month + 1)}-${
                                    listViewModel.addZero(day)
                                }"
                                /** onChange display value*/
                            }, year.toInt(), month.toInt() - 1, day.toInt()
                            /** initial display value*/
                        )
                        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
                        datePickerDialog.show()

                    }) {

                    Text(
                        text = date.value,
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.body2
                    )
                }

            Spacer(modifier = Modifier.size(8.dp))

            /** Item TextField */
            BasicTextField(
                value = item.value,
                onValueChange = {
                    if (it.length <= 50)
                        item.value = it
                },
                enabled = isSwipe.value,
                modifier = Modifier
                    .weight(0.7f)
                    .align(Alignment.CenterVertically),
                textStyle = MaterialTheme.typography.body2.copy(color = MaterialTheme.colors.onBackground),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    autoCorrect = true,
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next
                ),
            )

            Spacer(modifier = Modifier.size(4.dp))

            /** Price TextField */
            BasicTextField(
                value = price.value.toString(),
                onValueChange = {
                    if (it == "" || it.length > 7)
                        price.value = 0
                    else
                        price.value = it.toInt()
                },
                enabled = isSwipe.value,
                modifier = Modifier.weight(0.3f)
                    .align(Alignment.CenterVertically),
                textStyle = MaterialTheme.typography.body1
                    .copy(color = if (expensesMode) MaterialTheme.colors.error else MaterialTheme.colors.primaryVariant)
                    .copy(textAlign = if(isSwipe.value) TextAlign.Center else TextAlign.End),
               /*singleLine = true,*/
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    autoCorrect = false,
                    keyboardType = KeyboardType.NumberPassword,
                    imeAction = ImeAction.Next
                )
            )
        }

    }

    Divider(thickness = 0.5.dp)

    SwipeContainerList(
        isSwipe = isSwipe,
        deleteRow = deleteRow,
        updateRow = updateRow,
        scaffoldState = scaffoldState,
        snackbarController = snackbarController,
        listViewModel = listViewModel,
        cat = cat,
        application = application,
        expensesMode = expensesMode
    )

    /**For Same Date separation*/
    /**if current index date != next index date, then separate with space*/
    if (index < allEntities.size - 1 && allEntities[index].date != allEntities[index + 1].date) {
        Spacer(modifier = Modifier.size(8.dp))
    }
}

/**Group date, formatting date*/
@Composable
fun groupDateFormat(changeDateFormat: DateFormatModel) {
    Text(
        buildAnnotatedString {
            withStyle(style = SpanStyle(fontWeight = FontWeight.SemiBold, fontSize = 15.sp)) {
                append(changeDateFormat.day+" ")
            }
            withStyle(style = SpanStyle(fontWeight = FontWeight.Light,fontSize = 10.sp)) {
                append(changeDateFormat.month + "-" + changeDateFormat.year)
            }
        },
        color = MaterialTheme.colors.onBackground,
        fontFamily = QuickSand,
    )
}
/**For monthly only*/
@Composable
fun groupDateFormatMonth(changeDateFormat: DateFormatMonthModel) {
    Text(
        buildAnnotatedString {
            withStyle(style = SpanStyle(fontWeight = FontWeight.SemiBold, fontSize = 15.sp)) {
                append(changeDateFormat.month+" ")
            }
            withStyle(style = SpanStyle(fontWeight = FontWeight.Light,fontSize = 10.sp)) {
                append(changeDateFormat.year)
            }
        },
        color = MaterialTheme.colors.onBackground,
        fontFamily = QuickSand,
    )
}
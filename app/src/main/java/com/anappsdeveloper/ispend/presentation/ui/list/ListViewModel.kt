package com.anappsdeveloper.ispend.presentation.ui.list

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.anappsdeveloper.ispend.data.DateCategory.*
import com.anappsdeveloper.ispend.data.getFilterDateCategory
import com.anappsdeveloper.ispend.data.model.DateFormatModel
import com.anappsdeveloper.ispend.data.model.DateFormatMonthModel
import com.anappsdeveloper.ispend.data.model.DayMonthModel
import com.anappsdeveloper.ispend.presentation.MyApp
import com.anappsdeveloper.ispend.repository.MainRepository
import com.anappsdeveloper.ispend.room.MainEntity
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

const val PAGE_SIZE = 15

class ListViewModel
@ViewModelInject
constructor(private val mainRepository: MainRepository, private val application: MyApp) :
    ViewModel() {

    /**When chipset value changed. Changed occured in dailyEntities
    and its change will occurred to all where it is used because of mutablestateof*/
        var dailyEntities by mutableStateOf(listOf<DayMonthModel>()) 
        private set

    var monthlyEntities by mutableStateOf(listOf<DayMonthModel>())
        private set

    private val _allLiveData = MutableLiveData<List<MainEntity>>()
    val allLiveData:LiveData<List<MainEntity>> = _allLiveData

    private val _totalExpenses = MutableLiveData<Int>()
    val totalExpenses:LiveData<Int> = _totalExpenses

    private val _totalIncome = MutableLiveData<Int>()
    val totalIncome:LiveData<Int> = _totalIncome

    var selectedCategory by mutableStateOf(ALL)
        private set

    var filterScrollPosition: Float = 1f

    val loading = mutableStateOf(false)

    var query by mutableStateOf("")
        private set

    val page = mutableStateOf(1)
    val page_daily = mutableStateOf(1)

    val isSearchShown = mutableStateOf(false)

    init {
     fetchAllFromDB()
        fetchAllSum()
    }

    private fun fetchAllFromDB(){
        viewModelScope.launch {

            loading.value = true
          mainRepository.getAllMainEntity(limit = PAGE_SIZE, offSet = 0).collect {
              _allLiveData.value = it
              loading.value = false
          }
        }
    }

     private fun fetchAllSum(){
        viewModelScope.launch {
            loading.value = true

            mainRepository.getAllSum().collect{ list ->
                if(list.isNotEmpty())
                    /** if list[0]==true income = 0 else expenses = 0
                     * As while delete last one data of income or expenses, remains only one mode(income or expenses). so previous value remains as it instead of 0
                     * only one list means we have only one mode*/
               if(list[0].isExpenses)
                   _totalIncome.value = 0
               else
                   _totalExpenses.value = 0

                list.forEach {
                    if(it.isExpenses)
                        _totalExpenses.value = it.sum
                    else
                        _totalIncome.value = it.sum
                }
                loading.value = false
            }
        }
    }

    fun fetchDayMonthFromDB(){
        viewModelScope.launch {
            loading.value = true
            when (selectedCategory) {
                DAILY -> { dailyEntities = mainRepository.getDailyMainEntity(limit = PAGE_SIZE, offSet = 0) }
                MONTHLY -> {  monthlyEntities = mainRepository.getMontlyMainEntity() }
            }
            loading.value = false
        }
    }

    fun deleteMainEntityById(mainEntity: MainEntity) {
        viewModelScope.launch {
            mainRepository.deleteMainEntityById(mainEntity.id)
        }
    }

    fun updateMainEntityById(mainEntity: MainEntity) {
        viewModelScope.launch {
            mainRepository.updateMainEntityById(
                mainEntity.id,
                mainEntity.item,
                mainEntity.price,
                mainEntity.cat,
                mainEntity.date,
            )
        }
    }
  fun nextPage() {
      viewModelScope.launch {

          loading.value = true
          incrementPage()

          val offSet = (page.value - 1) * PAGE_SIZE //previous loaded list exclude

          mainRepository.getAllMainEntity(limit = PAGE_SIZE, offSet = offSet).collect {
              appendAllList(it)
              loading.value = false
          }
      }
  }

    fun nextPage_daily() {
        viewModelScope.launch {

            loading.value = true
            incrementPage()

            val offSet = (page_daily.value - 1) * PAGE_SIZE //previous loaded list exclude

            dailyEntities = mainRepository.getDailyMainEntity(limit = PAGE_SIZE, offSet = offSet)
            appendDailyList(dailyEntities)
            loading.value = false
        }
    }

    fun onSelectedCategoryChanged(category: String) {
        val newCategory = getFilterDateCategory(category)
        selectedCategory = newCategory!!
        clearQuery()

    }



    fun newSearch() {

        viewModelScope.launch {
            loading.value = true
            mainRepository.searchAllMainEntity(query).collect {
                _allLiveData.value = it
                loading.value = false
            }
        }

    }

    private fun clearQuery() {
        query = ""
    }

     fun clearSearch(){
        query = ""
        fetchAllFromDB()
         isSearchShown.value = false
    }

    fun onQueryChanged(query: String) {
        this.query = query
    }

    fun onListOrderChanged() {
        when (selectedCategory) {
            ALL -> _allLiveData.value = allLiveData.value?.reversed()
            DAILY -> dailyEntities = dailyEntities.reversed()
            MONTHLY -> monthlyEntities = monthlyEntities.reversed()
        }
    }

    private fun incrementPage() {
        if(selectedCategory == ALL)
        page.value += 1
        else
            page_daily.value += 1
    }

    private fun appendAllList(list: List<MainEntity>) {
        _allLiveData.value = allLiveData.value?.plus(list)
    }

    private fun appendDailyList(list:List<DayMonthModel>){
        val current = ArrayList(this.dailyEntities)
        current.addAll(list)
        this.dailyEntities = current
    }

    fun addZero(num: Int): String {
        return if (num < 10)
            "0$num"
        else
            "$num"
    }

    fun changeDateFormat(strDate: String): DateFormatModel {
        val strToken = StringTokenizer(strDate,"-")
        val year = strToken.nextToken()
        val month = strToken.nextToken()
        val day = strToken.nextToken()
        return DateFormatModel(day,month,year)
    }

    fun changeDateFormatMonth(strDate: String): DateFormatMonthModel {
        val strToken = StringTokenizer(strDate,"-")
        val year = strToken.nextToken()
        val month = strToken.nextToken()

        return DateFormatMonthModel(month,year)
    }
}
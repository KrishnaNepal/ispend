package com.anappsdeveloper.ispend.di

import android.content.Context
import com.anappsdeveloper.ispend.presentation.MyApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class) //exists as long as app is alive
object AppModule{
    @Provides
    fun provideApplication(@ApplicationContext context: Context): MyApp {
        return context as MyApp
    }
}
package com.anappsdeveloper.ispend.di

import com.anappsdeveloper.ispend.repository.MainRepository
import com.anappsdeveloper.ispend.room.MainDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule{

    @Provides
    fun provideMainRepository(mainDao: MainDao):MainRepository{ //constructor provided parameter, so spendDao is here; SpendDao object get from RoomModule
        return MainRepository(mainDao)
    }
}
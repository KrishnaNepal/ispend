package com.anappsdeveloper.ispend.di

import android.content.Context
import androidx.room.Room
import com.anappsdeveloper.ispend.room.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
object RoomModule {

    @Provides
    fun provideMainDataBase(@ApplicationContext context: Context) : MainDatabase{
        return Room.databaseBuilder(
            context,
            MainDatabase::class.java,
            MainDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideMainDao(mainDatabase: MainDatabase) : MainDao {
        return mainDatabase.mainDao()
    }
}
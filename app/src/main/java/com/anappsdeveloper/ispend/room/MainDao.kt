package com.anappsdeveloper.ispend.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.anappsdeveloper.ispend.data.model.AllSumModel
import com.anappsdeveloper.ispend.data.model.DayMonthModel
import kotlinx.coroutines.flow.Flow

@Dao
interface MainDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMainEntity(mainEntity: MainEntity): Long

    @Query("DELETE FROM MainEntity WHERE id = :id")
    suspend fun deleteMainEntityById(id: Int)

    @Query("UPDATE MainEntity SET item=:item, price=:price, cat=:cat, date=:date WHERE id = :id")
    suspend fun updateMainEntityById(
        id: Int,
        item: String?,
        price: Int?,
        cat: String?,
        date: String?
    )

    //Daily
    @Query("SELECT date,SUM(price) AS sum,isExpenses FROM MainEntity WHERE date >= DATE('now','-1 year') GROUP BY date,isExpenses ORDER BY date DESC LIMIT :limit OFFSET :offSet ")
    suspend fun getDailyMainEntity(limit: Int, offSet: Int): List<DayMonthModel>

    //Monthly
    @Query("SELECT strftime('%Y-%m', date) AS date, SUM(price) AS sum,isExpenses  FROM MainEntity GROUP BY strftime('%Y-%m', date),isExpenses ORDER BY date DESC ")
     suspend fun getMonthlyMainEntity(): List<DayMonthModel>

    //All
    @Query("SELECT * FROM MainEntity ORDER BY date DESC LIMIT :limit OFFSET :offSet")
     fun getAllMainEntity(limit: Int, offSet: Int): Flow<List<MainEntity>>

     //Search
    @Query("SELECT * FROM MainEntity WHERE item =:query ORDER BY date DESC")
    fun searchAllMainEntity(query:String): Flow<List<MainEntity>>

    //Total
    @Query("SELECT SUM(price) as sum, isExpenses FROM MainEntity GROUP BY isExpenses")
     fun getAllSum(): Flow<List<AllSumModel>>
}
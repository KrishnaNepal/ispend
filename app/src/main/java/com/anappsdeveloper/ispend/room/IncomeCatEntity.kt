package com.anappsdeveloper.ispend.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class IncomeCatEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int=0, /** here always put var for autoincrement */

    val name: String
)
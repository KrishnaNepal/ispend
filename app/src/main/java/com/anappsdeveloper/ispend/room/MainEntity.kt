package com.anappsdeveloper.ispend.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MainEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int=0, /** here always put var for autoincrement */

    val item: String,
    val price: Int,
    val cat: String,
    val date: String,
    val isExpenses:Boolean
)
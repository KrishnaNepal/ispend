package com.anappsdeveloper.ispend.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [MainEntity::class,ExpensesCatEntity::class, IncomeCatEntity::class],
    version = 3,exportSchema = false)
abstract class MainDatabase: RoomDatabase(){

    abstract fun mainDao(): MainDao

    companion object{
        const val DATABASE_NAME:String = "main_db"
    }
}
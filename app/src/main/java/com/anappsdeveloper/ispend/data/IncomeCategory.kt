package com.anappsdeveloper.ispend.data

enum class IncomeCategory(val value:String) {

    OTHER("Other"),
    SALARY("Salary"),
    WAGES("Wages"),
    PROFIT("Profit"),
    PARTTIME("Part-Time"),
}

fun getIncomeCategory(value:String): IncomeCategory?{
    val map = IncomeCategory.values().associateBy(IncomeCategory::value)
    return map[value]
}
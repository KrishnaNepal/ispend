package com.anappsdeveloper.ispend.data

enum class DateCategory(val value:String) {

    DAILY("Daily"),
    MONTHLY("Monthly"),
    ALL("All")
}

fun getFilterDateCategory(value:String): DateCategory?{
    val map = DateCategory.values().associateBy(DateCategory::value)
    return map[value]
}
package com.anappsdeveloper.ispend.data.model

data class DateFormatModel(
    val day:String,
    val month:String,
    val year:String
)
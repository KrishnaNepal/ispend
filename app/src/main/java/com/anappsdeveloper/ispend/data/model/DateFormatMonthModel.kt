package com.anappsdeveloper.ispend.data.model

data class DateFormatMonthModel(
    val month:String,
    val year:String
)
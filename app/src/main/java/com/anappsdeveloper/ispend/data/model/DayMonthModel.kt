package com.anappsdeveloper.ispend.data.model

data class DayMonthModel(
    val date:String,
    val sum:Int,
    val isExpenses:Boolean
)
package com.anappsdeveloper.ispend.data

enum class ExpensesCategory(val value:String) {

    MISC("Misc"),
    GROCERY("Grocery"),
    TRANSPORT("Transport"),
    LUNCH("Lunch"),
    SHOPPING("Shopping"),
    HEALTH("Health"),
    RENTBILLS("Rent & Bills"),
    EDUCATION("Education"),
}

fun getExpensesCategory(value:String): ExpensesCategory?{
    val map = ExpensesCategory.values().associateBy(ExpensesCategory::value)
    return map[value]
}

package com.anappsdeveloper.ispend.data.model

data class AllSumModel(
    val sum:Int,
    val isExpenses:Boolean
)
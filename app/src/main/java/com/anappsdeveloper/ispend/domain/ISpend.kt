package com.anappsdeveloper.ispend.domain

import java.util.*

data class ISpend(
    val item: String,
    val price:Int,
    val cat: String,
    val date: String,
    val isExpenses:Boolean,
    val id: UUID = UUID.randomUUID()
)